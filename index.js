require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const news = require('./routes/news');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

// router
app.use('/news', news);

if (process.env.PORT) {
  app.listen(process.env.PORT, () => console.log(`server running in port ${process.env.PORT}`));
} else {
  app.listen();
}