require('dotenv').config();
const axios = require('axios');
const pool = require('./db');
const parseString = require('xml2js').parseString;
const consoles = require('./logger');

async function getNews(category) {
  let queryInsert = '';
  const date = new Date;
  let insertCount = 0;
  try {
    const { rows } = await pool.query(`SELECT title FROM news WHERE category = '${category}' ORDER BY pub_date LIMIT 300;`);
    const { data } = await axios.get(process.env.BASE_URL_INEWS + category);
    parseString(data.replace(/'/g, '&apos;'), (err, res) => {
      if (err) throw err;
      const rssData = res.rss.channel[0].item;
      rssData.forEach(element => {
        const checkDataInDb = rows.find(el => el.title === element.title[0])
        if (!checkDataInDb) {
          queryInsert += `INSERT INTO news ( title, img, category, content, pub_date, created_at ) VALUES ( '${element.title[0]}', '${element.enclosure[0].$.url}', '${category}', '${element['content:encoded'][0]}', '${element.pubDate[0]}', '${date.toUTCString()}' );`;
        }
      });
    });
    if (queryInsert !== '') {
      const actionInsert = await pool.query(queryInsert);
      if (typeof actionInsert === 'object') {
        if (actionInsert.length > 0) {
          let rowCountArray = 0;
          actionInsert.forEach(el => {
            rowCountArray += el.rowCount;
          })
          insertCount = rowCountArray;
        } else {
          insertCount = actionInsert.rowCount;
        }
      }
    }
  } catch (error) {
    consoles.log(error);
  }
  return insertCount;
}

const deleteDuplicate = async () => {
  try {
    const { rowCount } = await pool.query('DELETE FROM news a USING news b WHERE a.id > b.id AND a.title = b.title');
    return rowCount;
  } catch (error) {
    consoles.log(error);
  }
}

const getAllNews = async () => {
  let dataCount = 0;
  const getDataNews = await getNews('news');
  consoles.log(`Category News = ${getDataNews}`);
  const getDataOtomotif = await getNews('otomotif');
  consoles.log(`Category Otomotif = ${getDataOtomotif}`); 
  const getDataLifestyle = await getNews('lifestyle');
  consoles.log(`Category Lifestyle = ${getDataLifestyle}`);
  const getDataTechno = await getNews('techno');
  consoles.log(`Category Techno = ${getDataTechno}`);
  const getDataTravel = await getNews('travel');
  consoles.log(`Categosy Travel = ${getDataTravel}`);
  const getDataSport = await getNews('sport');
  consoles.log(`Category Sport = ${getDataSport}`);
  const getDataFinance = await getNews('finance');
  consoles.log(`Category Finance = ${getDataFinance}`);
  const duplicateNews = await deleteDuplicate();
  consoles.log(`Duplicate News = ${duplicateNews}`);
  dataCount += getDataNews + getDataOtomotif + getDataLifestyle + getDataTechno + getDataTravel + getDataSport + getDataFinance - duplicateNews;
  consoles.log(`Berhasil menambahkan ${dataCount} data`);
  return(`Berhasil menambahkan ${dataCount} data`);
};

module.exports = getAllNews;