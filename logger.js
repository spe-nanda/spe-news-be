const moment = require('moment')
const fs = require('fs')
let logStream = fs.createWriteStream('log.txt')
let consoles = {}
consoles.log = (obj) => {
  var s = ''
  if (typeof obj === 'string')
    s = obj
  else
    s = JSON.stringify(obj)

  var dS = '[' + moment().format() + '] '
  s = `[${dS}] ${s}'\n'`
  logStream.write(s)
  console.log(dS);
}

module.exports = consoles;