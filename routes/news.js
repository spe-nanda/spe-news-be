const express = require('express');
const router = express.Router();
const axios = require('axios');
const pool = require('../db');
const consoles = require('../logger');
const { param, validationResult } = require('express-validator');
const syncNews = require('../add_news');

router.get('/', async (req, res) => {
  const page = parseInt(req.query.page) > 0 ? parseInt(req.query.page) : 1;
  const size = parseInt(req.query.size) > 0 ? parseInt(req.query.size) : 10;
  try {
    const totalNews = await pool.query('SELECT count(id) as total_news FROM public.news;');
    const newsCount = totalNews.rows[0].total_news;
    const totalPage = Math.ceil(newsCount / size);
    const { rows } = await pool.query(`SELECT * FROM news ORDER BY pub_date DESC LIMIT ${size} OFFSET ((${page} - 1) * ${size})`);
    const resNews = rows.map(el => {
      const { id, title, img, category, content } = el
      return {
        id,
        title,
        category,
        img,
        content
      }
    })
    res.json({
      current_page: page,
      size,
      total_page: totalPage,
      data: resNews
    })
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
});

router.get('/carousel', async (req, res) => {
  try {
    const { rows } = await pool.query(`SELECT * FROM news WHERE category = 'news' ORDER BY pub_date DESC LIMIT 5`);
    const resNews = rows.map(el => {
      const { id, title, img, category, content } = el
      return {
        id,
        title,
        category,
        img,
        content
      }
    })
    res.json({
      data: resNews
    })
  } catch (error) {
    res.sendStatus(500);
  }
});

router.get('/list-category', async (req, res) => {
  try {
    const { rows } = await pool.query('SELECT category FROM news GROUP BY category ORDER BY category');
    const queryResult = rows.map(el => {
      return el.category
    })
    res.json({
      all_category: queryResult
    })
  } catch (error) {
    res.sendStatus(500);
  }
})

router.get('/latest', async (req, res) => {
  try {
    const { rows } = await pool.query(`SELECT * FROM news ORDER BY pub_date DESC LIMIT 12`);
    const resNews = rows.map(el => {
      const { id, title, img, category, content } = el
      return {
        id,
        title,
        category,
        img,
        content
      }
    })
    res.json({
      data: resNews
    })
  } catch (error) {
    res.sendStatus(500);
  }
});

router.get('/category/:category', async (req, res) => {
  const page = parseInt(req.query.page) > 0 ? parseInt(req.query.page) : 1;
  const size = parseInt(req.query.size) > 0 ? parseInt(req.query.size) : 10;
  try {
    const totalNews = await pool.query(`SELECT count(id) as total_news FROM public.news WHERE category = '${req.params.category}';`);
    const newsCount = totalNews.rows[0].total_news;
    const totalPage = Math.ceil(newsCount / size);
    const { rows } = await pool.query(`SELECT * FROM news WHERE category = '${req.params.category}' ORDER BY pub_date DESC LIMIT ${size} OFFSET ((${page} - 1) * ${size});`);
    const resNews = rows.map(el => {
      const { id, title, img, category, content } = el
      return {
        id,
        title,
        category,
        img,
        content
      }
    })
    if (rows.length > 0) {
      res.json({
        current_page: page,
        size,
        total_page: totalPage,
        data: resNews
      })
    } else {
      res.status(404).json({ message: `Data untuk kategori ${req.params.category} tidak tersedia` })
    }
    
  } catch (error) {
    res.sendStatus(500);
  }
});

router.get('/sync', async (req, res) => {
  try {
    const syncResult = await syncNews();
    res.json({ message: syncResult });
  } catch (error) {
    res.sendStatus(500);
  }
});

router.get('/details/:id', param('id').isInt(), async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const { rows } = await pool.query(`SELECT * FROM news WHERE id = '${req.params.id}'`);
    const resNews = rows.map(el => {
      const { id, title, category, img, content } = el
      return {
        id,
        title,
        category,
        img,
        content
      }
    })
    if (rows.length > 0) {
      res.json({
        data: resNews[0]
      })
    } else {
      consoles.log('not-found');
      res.status(404).json({ message: 'Berita yang anda cari tidak ada' })
    }
    
  } catch (error) {
    res.status(500);
  }
});

router.get('/search', async (req, res) => {
  const query = req.query.query ? req.query.query : '';
  const page = parseInt(req.query.page) > 0 ? parseInt(req.query.page) : 1;
  const size = parseInt(req.query.size) > 0 ? parseInt(req.query.size) : 10;
  try {
    const totalNews = await pool.query(`SELECT COUNT(id) AS total_news FROM news WHERE LOWER(title) LIKE '%${query}%';`);
    const newsCount = totalNews.rows[0].total_news;
    const totalPage = Math.ceil(newsCount / size);
    const { rows } = await pool.query(`SELECT * FROM news WHERE LOWER(title) LIKE '%${query}%' ORDER BY pub_date DESC LIMIT ${size} OFFSET ((${page} - 1) * ${size})`);
    const resNews = rows.map(el => {
      const { id, title, img, category, content } = el
      return {
        id,
        title,
        category,
        img,
        content
      }
    })
    res.json({
      current_page: page,
      size,
      total_page: totalPage,
      data: resNews
    })
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
});

module.exports = router;